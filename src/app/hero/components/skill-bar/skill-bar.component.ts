import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'skill-bar',
  templateUrl: './skill-bar.component.html',
  styleUrls: ['./skill-bar.component.scss']
})
export class SkillBarComponent implements OnInit {

  @Input() public value: number;

  constructor() { }

  ngOnInit() {
  }

}
