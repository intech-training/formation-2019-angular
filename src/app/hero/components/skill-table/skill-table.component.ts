import { Component, OnInit, Input } from '@angular/core';

import { Hero } from '../../../shared/models/hero';

@Component({
  selector: 'skill-table',
  templateUrl: './skill-table.component.html',
  styleUrls: ['./skill-table.component.scss']
})
export class SkillTableComponent implements OnInit {

  @Input() public hero: Hero;

  skills = ['intelligence', 'strength', 'speed', 'durability', 'power', 'combat'];

  constructor() { }

  ngOnInit() {
  }

}
