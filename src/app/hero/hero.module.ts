import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { SkillBarComponent } from './components/skill-bar/skill-bar.component';
import { SkillTableComponent } from './components/skill-table/skill-table.component';
import { HeroRoutingModule } from './hero-routing.module';

@NgModule({
  declarations: [
    SkillTableComponent,
    SkillBarComponent
  ],
  imports: [
    HeroRoutingModule,
    CommonModule,
    SharedModule
  ],
  entryComponents: []
})
export class HeroModule { }
