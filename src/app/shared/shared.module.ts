import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FilterPipe } from './filter/filter.pipe';
import { MaterialModule } from './material.module';
import { MenubarComponent } from './menubar/menubar.component';

@NgModule({
  declarations: [
    MenubarComponent,

    FilterPipe
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,

    MenubarComponent,
    FilterPipe
  ]
})
export class SharedModule { }
