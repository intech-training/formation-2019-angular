import { NgModule } from '@angular/core';
import { ImageService } from './image.service';

@NgModule({
  declarations: [],
  imports: [],
  providers: [
    ImageService
  ]
})
export class CoreModule { }
